class AddCardsNumberToPacks < ActiveRecord::Migration[5.2]
  def change
    add_column :packs, :cards_number, :integer
  end
end
