class AddRoleToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_reference :players, :role, foreign_key: true
  end
end
