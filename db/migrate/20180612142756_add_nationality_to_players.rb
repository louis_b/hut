class AddNationalityToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_reference :players, :nationality, foreign_key: true
  end
end
