class Pack < ApplicationRecord
  has_and_belongs_to_many :players

  def buy(user)
    amount = user.credit - self.price
    if amount < 0
      return false
    end

    user.credit = amount
    user.save
  end

  def open
    available_players = self.players
    players_list = []
    for player in available_players do
      increment = 0
      while increment < (100 - player.rarity) do
        players_list.push(player.id)
        increment += 1
      end
    end
    players_list = players_list.shuffle

    pack_players = []
    increment = 0
    while increment < self.cards_number do
      pack_players.push(Player.find(players_list.sample))
      increment += 1
    end

    pack_players
  end
end
