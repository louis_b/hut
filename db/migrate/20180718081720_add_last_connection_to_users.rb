class AddLastConnectionToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :last_connection, :date
  end
end
