class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]

  # GET /teams
  def index
    @teams = current_user.teams
  end

  # GET /teams/1
  def show
    @team = Team.find(params[:id])
    @players = @team.players

    respond_to do |format|
      format.html
    end
  end

  # GET /teams/new
  def new
    @team = Team.new
    @players = current_user.players.distinct
  end

  # GET /teams/1/edit
  def edit
    @players = current_user.players.distinct
  end

  # POST /teams
  def create
    params[:team][:player_ids] ||= []
    params[:team][:active] ||= false
    @team = Team.new(team_params)
    @team.user_id = current_user.id

    if team_params['active']
      disable_teams(-1)
    end

    respond_to do |format|
      if params[:team][:player_ids].length > 7
        format.html { redirect_to new_team_url, notice: "Vous pouvez seulement utiliser 7 joueurs dans votre équipe, ca n'est pas du rugby" }
      elsif @team.save
        format.html { redirect_to @team, notice: 'Team was successfully created.' }
      else
        format.html { redirect_to new_team_url, notice: 'Merci de renseigner le nom de l\'équipe.' }
      end
    end
  end

  # PATCH/PUT /teams/1
  def update
    params[:team][:player_ids] ||= []
    params[:team][:active] ||= false
    @team = Team.find(params[:id])

    if team_params['active']
      disable_teams(@team.id)
    end

    respond_to do |format|
      if params[:team][:player_ids].length > 7
        format.html { redirect_to edit_team_url, notice: "Vous pouvez seulement utiliser 7 joueurs dans votre équipe, ca n'est pas du rugby" }
      elsif @team.update(team_params)
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
      else
        format.html { redirect_to edit_team_url, notice: 'Merci de renseigner le nom de l\'équipe.' }
      end
    end
  end

  # DELETE /teams/1
  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name, :active, :player_ids => [])
    end

    def disable_teams(active_team_id)
      user_teams = Team.where(["user_id = :u", { u: current_user.id }])
      user_teams.each do |team|
        if active_team_id != team.id
          team.active = false
          team.save
        end
      end
    end
end
