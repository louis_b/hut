# README

### Pré-requis pour run le projet
ruby, Rails.

### Comment lancer le projet

1 - Git clone le projet et ouvrir un terminal à la racine du projet
```
git clone git@gitlab.com:louis_b/hut.git
```
2 - Mettre à jour les dépendances avec la commande :
```
bundle install
```
3 - Exécuter les migrations avec la commande :
```
rails db:migrate
```
4 - Importer les données dans le projet depuis la seed avec la commande :
```
rails db:seed
```
3 - Lancer l'application avec la commande :
```
rails s
```

Voilà tout est opérationnel -> http://localhost:3000