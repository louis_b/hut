class Nationality < ApplicationRecord
  has_many :players
  has_one_attached :avatar
end
