class AddNoteToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :note, :integer
  end
end
