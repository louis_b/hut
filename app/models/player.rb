class Player < ApplicationRecord
  has_and_belongs_to_many :packs
  has_and_belongs_to_many :teams
  belongs_to :club
  belongs_to :nationality
  belongs_to :role
  has_one_attached :avatar

  def sell(user)
    # remove the player from association table
    sql = "DELETE FROM players_users WHERE user_id=#{user.id} AND player_id=#{self.id} LIMIT 1"
    ActiveRecord::Base.connection.execute(sql)

    user.credit = user.credit + self.price
    user.save
  end
end
