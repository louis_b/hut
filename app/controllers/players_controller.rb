class PlayersController < ApplicationController
  before_action :set_player, only: [:show, :edit, :update, :destroy, :sell]

  def index
    @players = Player.all
  end

  def my_players
    @players = current_user.players
  end

  def show
  end

  def sell
    @player.sell(current_user)

    redirect_to my_players_url, notice: "#{@player.name} a bien été vendu"
  end

  def new
    @player = Player.new
  end

  def edit; end

  def create
    @player = Player.new(player_params)

    if @player.save
      redirect_to @player, notice: 'Player was successfully created.'
    else
      render :new
    end
  end

  def update
    if @player.update(player_params)
      redirect_to @player, notice: 'Player was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @player.destroy
    redirect_to players_url, notice: 'Player was successfully destroyed.'
  end

  private
    def set_player
      @player = Player.find(params[:id])
    end

    def player_params
      params.require(:player).permit(:name, :speed, :strength, :defense, :shoot, :rarity, :avatar, :note, :price, :role_id, :club_id, :nationality_id)
    end
end
