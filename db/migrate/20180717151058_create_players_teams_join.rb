class CreatePlayersTeamsJoin < ActiveRecord::Migration[5.2]
  def self.up
    create_table :players_teams do |t|
      t.integer :player_id
      t.integer :team_id
    end
  end

  def self.down
    drop_table :players_teams
  end
end
