Rails.application.routes.draw do
  root 'home#index'
  devise_for :users
  authenticate :user do
    get 'users/index'
    get 'users/match/:user_id' => 'users#match', as: :match
    resources :teams
    resources :nationalities
    resources :leagues
    resources :clubs
    resources :roles
    resources :players do
      get 'sell', on: :member
      get 'my/players', action: :my_players, as: :my, on: :collection
    end
    resources :packs do
      get 'list', on: :collection
      get ':user_id/buy', action: :buy, as: :buy, on: :member
    end
  end
end
