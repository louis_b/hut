class League < ApplicationRecord
  has_many :clubs
  has_one_attached :avatar
end
