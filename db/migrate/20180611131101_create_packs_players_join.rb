class CreatePacksPlayersJoin < ActiveRecord::Migration[5.2]
  def self.up
    create_table 'packs_players', :id => false do |t|
      t.column 'pack_id', :integer
      t.column 'player_id', :integer
    end
  end

  def self.down
    drop_table 'packs_players'
  end
end
