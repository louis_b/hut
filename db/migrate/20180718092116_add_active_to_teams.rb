class AddActiveToTeams < ActiveRecord::Migration[5.2]
  def change
    add_column :teams, :active, :boolean
  end
end
