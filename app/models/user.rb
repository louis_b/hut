class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :players
  has_many :teams

  def after_database_authentication
    if self.last_connection.nil? || !self.last_connection.to_s.start_with?(Date.today.to_s)
      self.update_attributes(:last_connection => Date.today.to_s)
      self.credit += 1000
    end
  end

  def fight(user)
    success = rand(10) > rand(10)

    #TODO : calculate winner based on users active team players

    if success
      # attribute match reward in case of win
      self.credit = self.credit + 500
      self.save
    end
  end
end
