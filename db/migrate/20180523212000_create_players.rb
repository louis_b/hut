class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string :name
      t.integer :speed
      t.integer :strength
      t.integer :defense
      t.integer :shoot
      t.integer :rarity

      t.timestamps
    end
  end
end
