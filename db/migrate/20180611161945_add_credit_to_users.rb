class AddCreditToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :credit, :integer, default: 3000
  end
end
