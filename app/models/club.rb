class Club < ApplicationRecord
  has_many :players
  belongs_to :league
  has_one_attached :avatar
end
