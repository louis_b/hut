class NationalitiesController < ApplicationController
  before_action :set_nationality, only: [:show, :edit, :update, :destroy]

  # GET /nationalities
  def index
    @nationalities = Nationality.all
  end

  # GET /nationalities/1
  def show
  end

  # GET /nationalities/new
  def new
    @nationality = Nationality.new
  end

  # GET /nationalities/1/edit
  def edit
  end

  # POST /nationalities
  def create
    @nationality = Nationality.new(nationality_params)

    respond_to do |format|
      if @nationality.save
        format.html { redirect_to @nationality, notice: 'Nationality was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /nationalities/1
  def update
    respond_to do |format|
      if @nationality.update(nationality_params)
        format.html { redirect_to @nationality, notice: 'Nationality was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /nationalities/1
  def destroy
    @nationality.destroy
    respond_to do |format|
      format.html { redirect_to nationalities_url, notice: 'Nationality was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nationality
      @nationality = Nationality.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nationality_params
      params.require(:nationality).permit(:name, :logo, :avatar)
    end
end
