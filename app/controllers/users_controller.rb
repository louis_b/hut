class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def match
    @opponent = User.find(params[:user_id])
    success = current_user.fight(@opponent)

    respond_to do |format|
      if success
        format.html { redirect_to users_index_path, notice: 'Vous avez gagné, félicitation ! Vous venez de recevoir 500 crédits' }
      else
        format.html { redirect_to users_index_path, notice: 'Vous avez perdu, continuez de renforcer votre équipe et vous ne connaîtrez plus ce goût amer de la défaite' }
      end
    end
  end
end
