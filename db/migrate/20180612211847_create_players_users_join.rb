class CreatePlayersUsersJoin < ActiveRecord::Migration[5.2]
  def self.up
    create_table 'players_users', :id => false do |t|
      t.column 'user_id', :integer
      t.column 'player_id', :integer
    end
  end

  def self.down
    drop_table 'players_users'
  end
end
