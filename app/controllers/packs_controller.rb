class PacksController < ApplicationController
  before_action :set_pack, only: [:show, :edit, :update, :destroy]

  # GET /packs
  def index
    @packs = Pack.all
  end

  def list
    @packs = Pack.all
    @isBuy = true

    render 'index'
  end

  def buy
    @pack = Pack.find(params[:id])
    @user = User.find(params[:user_id])
    success = @pack.buy(@user)
    if success
      @pack_players = @pack.open
      for player in @pack_players do
        @user.players << player
      end
    end

    respond_to do |format|
      if success
        format.html { render :buy, notice: 'La pack a bien été acheté.' }
      else
        format.html { redirect_to list_packs_path, notice: 'Vous ne pouvez pas acheter ce pack.' }
      end
    end
  end

  # GET /packs/1
  def show
    @pack = Pack.find(params[:id])
    @players = @pack.players

    respond_to do |format|
      format.html
    end
  end

  # GET /packs/new
  def new
    @pack = Pack.new
    @players = Player.all
  end

  # GET /packs/1/edit
  def edit
    @players = Player.all
  end

  # POST /packs
  def create
    @pack = Pack.new(pack_params)

    respond_to do |format|
      if @pack.save
        format.html { redirect_to @pack, notice: 'Pack was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /packs/1
  def update
    params[:pack][:player_ids] ||= []
    @pack = Pack.find(params[:id])

    respond_to do |format|
      if @pack.update(pack_params)
        format.html { redirect_to @pack, notice: 'Pack was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /packs/1
  def destroy
    @pack.destroy
    respond_to do |format|
      format.html { redirect_to packs_url, notice: 'Pack was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pack
      @pack = Pack.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pack_params
      params.require(:pack).permit(:name, :price, :cards_number, :player_ids => [])
    end
end
